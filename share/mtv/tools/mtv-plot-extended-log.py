#!/usr/bin/env python

############################################################################
# Copyright (C) 2016 by gempa GmbH                                         #
#                                                                          #
# All Rights Reserved.                                                     #
#                                                                          #
# NOTICE: All information contained herein is, and remains                 #
# the property of gempa GmbH and its suppliers, if any. The intellectual   #
# and technical concepts contained herein are proprietary to gempa GmbH    #
# and its suppliers.                                                       #
# Dissemination of this information or reproduction of this material       #
# is strictly forbidden unless prior written permission is obtained        #
# from gempa GmbH.                                                         #
#                                                                          #
#  Author: Lukas Lehmann, Dirk Roessler                                    #
#  Email: roessler@gempa.de                                                #
############################################################################


import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.patches as mpatches
import seiscomp3.Client
from seiscomp3 import System
import mplstereonet # pip install mplstereonet
import sys
from dateutil.parser import parse
import datetime
import os
import numpy as np
import subprocess
import argparse
import webbrowser

# set default figure parameters
figParams = {'legend.fontsize': 10,
          'figure.figsize': (10, 10),
          'axes.labelsize': 10,
          'axes.titlesize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10}
plt.rcParams.update(figParams)

###############################################################################
'''
plotgenerator
Return: figurePathDict
'''

phasedict = {
        'P': {'phase':'P-phase', 'wave':'body wave'},
        'S': {'phase':'S-phase','wave':'body wave'},
        'L': {'phase':'Love-wave','wave':'surface wave'},
        'R': {'phase':'Rayleigh-wave','wave':'surface wave'},
        'LM': {'phase':'LM-phase','wave':'mantel phase'},
        'RM': {'phase':'RM-phase','wave':'mantel phase'},
        'W' : {'phase':'W-phase','wave':''},
        'FULL': {'phase':'full-waveform', 'wave':''}
        }

sc3datadir = System.Environment_Instance().absolutePath("@DATADIR@")
companyLogo = os.path.join(sc3datadir, "doc/mt/html/_static/gempa.png")

## Plot of the Moment Tensor, principal axes and polarities
## Return: figurePathDict
def plot_mt(args, linksDict, parameters, plotpath):
    from matplotlib.legend_handler import HandlerPatch
    from matplotlib import collections, patches, transforms

    class HandlerEllipse(HandlerPatch):
        def create_artists(self, legend, orig_handle,
                           xdescent, ydescent, width, height, fontsize, trans):
            center = 0.5 * width - 0.5 * xdescent, 0.5 * height - 0.5 * ydescent
            p = mpatches.Ellipse(xy=center, width=height + xdescent,
                                 height=height + ydescent)
            self.update_prop(p, orig_handle, legend)
            p.set_transform(trans)
            return [p]

    directory = args.inputfile
    figureLinks = linksDict


    strike1 = float(parameters['---FM-NP1S---'])
    dip1 = float(parameters['---FM-NP1D---'])
    rake1 = float(parameters['---FM-NP1R---'])

    strike2 = float(parameters['---FM-NP2S---'])
    dip2 = float(parameters['---FM-NP2D---'])
    rake2 = float(parameters['---FM-NP2R---'])

    pazim = parameters['---FM-PAZIM---']
    tazim = parameters['---FM-TAZIM---']
    nazim = parameters['---FM-NAZIM---']
    pplunge = parameters['---FM-PPLUNGE---']
    tplunge = parameters['---FM-TPLUNGE---']
    nplunge = parameters['---FM-NPLUNGE---']

    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(111, projection='stereonet')

    ### plot polarity picks, absolutly wrong!! needed is the takeoff angle and not the distance
    # for ii in parameters['---PICKS---']:
    #     azim = parameters['---PICKS---'][ii]['azimuth']
    #     dist = parameters['---PICKS---'][ii]['distance']
    #     phase = parameters['---PICKS---'][ii]['phase']
    #     polarity = parameters['---PICKS---'][ii]['polarity']
    #     phaseRes = parameters['---PICKS---'][ii]['residuum']

    #     if phaseRes < 0.5:
    #         edgecolor = 'green'
    #     elif phaseRes < 1.:
    #         edgecolor = 'yellow'
    #     else:
    #         edgecolor = 'red'

    #     if polarity == None:
    #         fillcolor = 'None'
    #         marker = 'x'
    #         edgecolor = 'black'
    #     elif polarity == '+':
    #         fillcolor = 'black'
    #         marker = 'o'
    #     elif polarity == '-':
    #         fillcolor = 'None'
    #        marker = 'o'
    #     ax.line(dist,azim, marker=marker, color=fillcolor, markeredgecolor=edgecolor)

    ### plot principal axis and nodal planes
    size = 10.
    pax = ax.line(pplunge, pazim, markersize=size, marker='^', markerfacecolor='black', markeredgecolor='black', label='P-Axis' )
    tax = ax.line(tplunge, tazim, markersize=size, marker='^', markerfacecolor='none', markeredgecolor='black', label='T-Axis')
    # nax = ax.line(nplunge, nazim, markersize=size, marker='^', markercolor='green', label='N-Axis')

    pl1, = ax.plane(strike1, dip1, 'k:', label='NP1: %s %s' % (strike1, dip1))
    pl2, = ax.plane(strike2, dip2, 'k:', label='NP2: %s %s' % (strike2, dip2))

    handle, label = ax.get_legend_handles_labels()
    no_pol = [mpatches.Patch(hatch='xxx', facecolor='None')]
    pos_pol = [ mpatches.Circle((0.5, 0.5), radius = 0.25, facecolor='black', edgecolor='green' )]
    neg_pol = [ mpatches.Circle((0.5, 0.5), radius = 0.25, facecolor='none', edgecolor='green' )]

    pol_labels = ['No Polarity', '+ Polarity', '- Polarity']
    handles = no_pol + pos_pol + neg_pol + handle
    labels = pol_labels + label

    # ax.legend(handles=handles, labels=labels, ncol=3,
    #           handler_map={mpatches.Circle: HandlerEllipse()},
    #           loc='lower center', bbox_to_anchor=(0.5, -0.15))

    savename = '%s/mt_solution.png' %plotpath
    fig.savefig(('%s' %(savename)), bbox_inches='tight', dpi = 300)
    plt.close(fig)
    print >> sys.stdout, "Saving MT plot to ", savename

    figureLinks['---mtsolution-link---'] = savename
    linksDict.update(figureLinks)

    return linksDict


## Generate a map with the location of the earthquake and the used stations
## Return: figurePathDict
def generateMap(args, linksDict, parameters, plotpath):

    figureLinks = linksDict
    directory = args.inputfile
    xmlFile = os.path.join(directory, 'event.xml')
    savename = '%s/map.png' %plotpath

    # map using matpliblib
    # from mpl_toolkits import basemap
    # evlat = float(parameters['---EVORI-LATITUDE---'])
    # evlong = float(parameters['---EVORI-LONGITUDE---'])
    # cenlat = float(parameters['---CENTROID-LATITUDE---'])
    # cenlong = float(parameters['---CENTROID-LONGITUDE---'])
    # m = basemap.Basemap(projection='ortho',lat_0=evlat,lon_0=evlong,
    #                         resolution='l')
    #
    # fig = plt.figure(figsize=(10,5))
    # x,y = m(evlong,evlat)
    # m.plot(x,y, 'r*', markersize=20.)
    # m.drawmeridians(range(0,360,30))
    # m.drawparallels(range(-90,90,30))
    # m.drawcountries(linewidth=0.25)
    # m.drawcoastlines(linewidth=0.25)
    # m.fillcontinents(color='#D1C0BA',lake_color="#6D959F")
    # m.drawmapboundary(fill_color="#6D959F")
    # plt.tight_layout()
    # fig.savefig(('%s' %(savename)), bbox_inches='tight')
    # plt.close(fig)

    # map using scmapcut
    # scmapcut --ep xmlFile -m 90 -d 400x400 -o savename
    cmdCreatePng = "scmapcut --ep %s -m 180 -d 800x800 -o %s" %(xmlFile,savename)
    print >> sys.stdout, "executing: ", cmdCreatePng
    process = subprocess.Popen(cmdCreatePng.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    print >> sys.stdout, "Saving map plot to ", savename

    figureLinks['---map-link---'] = savename
    linksDict.update(figureLinks)

    return linksDict


## Read in the depthsearch file, if existing
## Create different plots as function of the depth
## Return: figurePathDict
def depthSearch(args, linksDict, parameters, plotpath):
    figureLinks = linksDict
    directory = args.inputfile
    fileName = '%s/depthsearch.tbl' %directory
    if os.path.exists(fileName):
        depths = []
        fits = []
        dcs = []
        clvds = []
        isos = []
        Mws = []
        nStas = []
        qualities = []

        markers = []
        with open(fileName) as file:
            for line in file:
                if line.startswith('#'):
                        continue
                line = line.rsplit()

                depths.append(float(line[0]))
                fits.append(float(line[1]))
                depth = float(line[0])
                fit = float(line[1])
                dcs.append(float(line[2]))
                clvds.append(float(line[3]))
                try:
                    isos.append(float(line[4]))
                except:
                    isos.append(0.0)
                Mws.append(float(line[5]))
                nStas.append(int(line[6]))
                qualities.append(float(line[1])*float(line[6]))
                np1Str = float(line[7])
                np1Dip = float(line[8])
                np1Rake = float(line[9])
                np2Str = float(line[10])
                np2Dip = float(line[11])
                np2Rake = float(line[12])

        finalDepth = float(parameters['---CENTROID-DEPTH---'])
        vlinelabel = 'centroid'
        minDepthPlot = min(depths) - 0.01*(max(depths)-min(depths))
        maxDepthPlot = max(depths) + 0.01*(max(depths)-min(depths))

        fig = plt.figure('DepthSearch', figsize=(10,10))
        ax = plt.subplot(311)
        # ax.set_title('Depth Search')
        ax2 = ax.twinx()
        pl1, = ax.plot(depths, fits, c='black', linestyle='-', marker='o', label='fit')
        pl2, = ax2.plot(depths, qualities, c='green', linestyle=':', marker='o', label='quality')

        ax.set_xlim([minDepthPlot, maxDepthPlot])
        ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))

        ax2.axvline(x=finalDepth, label=vlinelabel)
        ax.set_ylabel('fit [%]')
        ax2.set_ylabel('quality')
        ax.yaxis.label.set_color(pl1.get_color())

        lines, labels = ax.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax2.legend(lines + lines2, labels + labels2)

        ax = plt.subplot(312)
        ax2 = ax.twinx()
        pl1, = ax.plot(depths, dcs, c='black', linestyle='-', marker=(4,3,10), label='DC')
        pl2, = ax2.plot(depths, clvds, c='green', linestyle=':', marker='o', label='CLVD')
        pl3, = ax2.plot(depths, isos, c='red', linestyle=':', marker='o', label='ISO')
        ax2.axvline(x=finalDepth, label=vlinelabel)

        ax.set_ylabel('DC [%]')
        ax2.set_ylabel('CLVD / ISO [%]')
     
        ax.set_xlim([minDepthPlot, maxDepthPlot])
        ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))

        ax.set_ylim([-100,100])
        ax.yaxis.grid(True)
        ax2.set_ylim([-100,100])
        ax.yaxis.label.set_color(pl1.get_color())
        lines, labels = ax.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax2.legend(lines + lines2, labels + labels2)

        ax = plt.subplot(313)
        ax2 = ax.twinx()
        pl1, = ax.plot(depths, Mws, c='black', linestyle='-', marker='o', label='magnitude')
        ax.axvline(x=finalDepth, label=vlinelabel)
        pl2, = ax2.plot(depths, nStas, c='green', linestyle=':', marker='o', label='station count')
        ax.set_ylabel('magnitude')
        ax.set_xlabel('depth [km]')

        ax.set_xlim([minDepthPlot,maxDepthPlot])
#        ax2.set_xlim([minDepthPlot,maxDepthPlot])
#        ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())

#        ax.set_xlim([minDepthPlot, maxDepthPlot])
#        ax2.xaxis.set_major_locator(plt.MultipleLocator(50))
#        ax2.xaxis.set_minor_locator(plt.MultipleLocator(10))
        ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
#        ax2.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
        
        ax.yaxis.label.set_color(pl1.get_color())
        # set ticks
        if max(nStas) == min(nStas):
            nTicks = 1
        else:
            ax2.yaxis.set_minor_locator(ticker.AutoMinorLocator())
            ax2.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))
            if max(nStas) - min(nStas) < 4:
                nTicks = max(nStas) - min(nStas)
            else:
                nTicks = 4

            if max(nStas) - min(nStas) % 2 == 0:
                nTicks += 1
            else:
                nTicks += 2

        ax2.set_yticks(np.linspace(min(nStas), max(nStas), nTicks, endpoint=True))
        # set labls
        ax2.set_ylabel('used stations')
        lines, labels = ax.get_legend_handles_labels()
        lines2, labels2 = ax2.get_legend_handles_labels()
        ax2.legend(lines + lines2, labels + labels2)

        plt.tight_layout()
        savename = '%s/depthsearch.png' %plotpath
        fig.savefig(('%s' %(savename)), bbox_inches='tight', dpi=300)
        plt.close(fig)
        print >> sys.stdout, "Saving depth plot to ", savename

        figureLinks['---depthsearch-link---'] = savename
        linksDict.update(figureLinks)

    else:
        print 'Depthsearch.tbl is not existing'
        figureLinks['---depthsearch-link---'] = 'not available'
        linksDict.update(figureLinks)

    return linksDict


## Read one waveform file
## Return: unmodified Amplitudes of observed and synthetic data, time data
def plottraces(file):
    amp1 = []
    amp2 = []
    for line in file:
        line = line.rsplit()

        if line[0].startswith('#'):
            if line[1].startswith('dt='):
                dt = float(line[1].replace('dt=','').replace('s',''))
                nSample = int(line[2].replace('n=',''))

                time = [ii/dt for ii in range(0, nSample)]
            else:
                if 'm' == line[1].replace('dist=','')[-1]:
                    dist = float(line[1].replace('dist=','').replace('km',''))
                    distUnit = 'km'
                elif 'g' == line[1].replace('dist=','')[-1]:
                    dist = float(line[1].replace('dist=','').replace('deg',''))
                    distUnit = 'deg'
                else:
                    print line[1].replace('dist=','')
                    print 'No distance found'
                    exit()
            continue

        amp1.append(float(line[0]))
        amp2.append(float(line[1]))

    return amp1, amp2, time, dist, distUnit

## Plot the waveform data by distance for every phasetype
## Return: figurePathDict
def distplot(args, linksDict, plotpath, parameters):
    figureLinks = linksDict
    figureLinks['---distplot-link---'] = {}
    directory = args.inputfile
    figNames = set()
    # set figure height in cm
    # figHeight = figParams['figure.figsize'][1]

    # get the maximum distance for the plots
    distances = []
    for fileName in os.listdir(directory):
        if fileName.endswith('.data'):
            file = '%s/%s' %(directory,fileName)
            f = open(file,'r')
            line = f.readlines()[1].rsplit(' ')
            f.close()

            if 'm' == line[1].replace('dist=','')[-1]:
                dist = float(line[1].replace('dist=','').replace('km',''))
                distUnit = 'km'
            elif 'g' == line[1].replace('dist=','')[-1]:
                dist = float(line[1].replace('dist=','').replace('deg',''))
                distUnit = 'deg'
            else:
                print line[1].replace('dist=','')
                print 'No distance found in file ' + file
                exit()
            distances.append(dist)
    maxDist = max(distances)

    # create the distance plots
    xlims = []
    for fileName in os.listdir(directory):
        if fileName.endswith('.data'):
            with open('%s/%s' %(directory,fileName)) as file:
                fileName = fileName.replace('.data','').rsplit('_')
                for phase in phasedict.keys():
                    if phase == fileName[-1]:
                        component = fileName[-2]
                        wavetyp = phasedict[phase]['wave']
                        phasetyp = phasedict[phase]['phase']
                        figName = '%s %s %s' %(wavetyp, phasetyp, component)
                        figNames.add("%s,%s,%s" % (figName,wavetyp,phasetyp))
                        fig = plt.figure(figName, figsize=(10,10))
                        amp1, amp2, time, dist, distUnit = plottraces(file)

                        # scale to 1cm for max - min height of traces
                        amps = [amp1,amp2]
                        scale = (max(max(amps)) - min(min(amps))) * 2 * figParams['figure.figsize'][1] / maxDist
                        amp1[:] = [x/scale for x in amp1]
                        amp2[:] = [x/scale for x in amp2]

                        xlim = max(time)
                        xlims.append(xlim)
                        distances.append(dist)
                        plt.plot(time, [(x+dist) for x in amp2], color='r', linewidth=1, clip_on=False)
                        plt.plot(time, [(x+dist) for x in amp1], color='k', linewidth=1, alpha=0.75, clip_on=False)
                        stationID = "%s.%s" %(fileName[0],fileName[1])
                        plt.text(xlim*1.01, dist, stationID, fontsize=10)
                        if distUnit == "deg":
                            plt.ylabel('Epicentral Distance [$^\circ$]')
                        else:
                            plt.ylabel('Epicentral Distance [km]')
    # save the distance plots:
    for name in figNames:
        figName = name.split(',')[0]
        component = figName[-1]
        phasetyp = name.split(',')[2]
        fig = plt.figure(figName)
        plt.ylim([0,maxDist*1.05])
        plt.xlim([0,max(xlims)])
        plt.title(figName)
        plt.xlabel('Time relative to P Arrival [s]')
        plt.legend(['synthetic data', 'observed data'], loc='lower center', ncol=2, bbox_to_anchor=(0.5, -0.12))
        savename = '%s/%s-%s.png' %(plotpath, phasetyp, component)
        print >> sys.stdout, "Saving distance plot to ", savename
        fig.savefig(('%s' %(savename)), dpi = 300)
        plt.close(fig)

        figureLinks['---distplot-link---']['%s %s' % (phasetyp,component)] = savename

    return linksDict


## Plot for every station a grid-like structure of the waveform data sorted by
## existing phases and components
## Return: figurePathDict
def singleStationPlot(args, linksDict, plotpath):
    figureLinks = linksDict
    figureLinks['---singleStation-link---'] = {}
    directory = args.inputfile

    fileDict = {}

    clmDict = {'Z':0, 'R':1, 'T':2}
    for fileName in os.listdir(directory):
        if fileName.endswith('.data'):
            filePrefix = fileName.replace('.data','').rsplit('_')
            net = filePrefix[0]
            sta = filePrefix[1]
            comp = filePrefix[2]
            phas = filePrefix[3]

            code = '%s_%s' %(net,sta)
            if code not in fileDict:
                fileDict[code] = {}
                fileDict[code]['types'] = []
                fileDict[code]['Phases'] = []

            fileDict[code]['types'].append(comp + '-' + phas)
            fileDict[code]['counter'] = 0
            fileDict[code]['bin'] = []
            fileDict[code]['Phases'].append(phas)


    for code in fileDict:
        phaseList = fileDict[code]['Phases']
        if 'P' in phaseList or 'S' in phaseList:
            if 'P' not in fileDict[code]['bin']:
                fileDict[code]['bin'].append('P')
                fileDict[code]['counter'] += 1
        if 'R' in phaseList or 'L' in phaseList:
            if 'R' not in fileDict[code]['bin']:
                fileDict[code]['bin'].append('R')
                fileDict[code]['counter'] += 1
        if 'RM' in phaseList or 'LM' in phaseList:
            if 'RM' not in fileDict[code]['bin']:
                fileDict[code]['bin'].append('RM')
                fileDict[code]['counter'] += 1
        if 'W' in phaseList:
            if 'W' not in fileDict[code]['bin']:
                fileDict[code]['bin'].append('W')
                fileDict[code]['counter'] += 1
        if 'FULL' in phaseList:
            if 'FULL' not in fileDict[code]['bin']:
                fileDict[code]['bin'].append('FULL')
                fileDict[code]['counter'] += 1

    for code in fileDict:
        cnt = fileDict[code]['counter']

        phaseList = fileDict[code]['Phases']

        cnt2 = 0
        rowDict = {}
        if 'P' in phaseList or 'S' in phaseList:
            rowDict['P'] = cnt2
            rowDict['S'] = cnt2
            cnt2 += 1
        if 'R' in phaseList or 'L' in phaseList:
            rowDict['R'] = cnt2
            rowDict['L'] = cnt2
            cnt2 += 1
        if 'RM' in phaseList or 'LM' in phaseList:
            rowDict['RM'] = cnt2
            rowDict['LM'] = cnt2
            cnt2 += 1
        if 'W' in phaseList:
            rowDict['W'] = cnt2
            cnt2 += 1
        if 'FULL' in phaseList:
            rowDict['FULL'] = cnt2
            cnt2 += 1

        for ii in range(len(fileDict[code]['types'])):
            component = fileDict[code]['types'][ii].rsplit('-')[0]
            phasetype = fileDict[code]['types'][ii].rsplit('-')[1]

            clmindex = clmDict[component]
            rowindex = rowDict[phasetype]

            num = 3 * rowindex + (clmindex) +1
            fileName = '%s_%s_%s.data' %(code, component, phasetype)
            file = open('%s/%s' %(directory,fileName), 'r')
            amp1, amp2, time, dist, distUnit = plottraces(file)
            file.close()

            fig = plt.figure('%s' %code, figsize=(10, cnt*3))
            # plt.suptitle('%s' %code)

            ax = plt.subplot(cnt, 3, num)
            ax.plot(time, amp2, 'r', linewidth=3)
            ax.plot(time, amp1, 'k', linewidth=1)
            if distUnit == "deg":
                unit = '$^\circ$'
            else:
                unit = ' km'
            ax.set_title('%s: %.1f%s, %s, %s' %(code.replace('_','.'),dist, unit, phasedict[phasetype]['phase'], component))

            if clmindex == 0:
                ax.set_ylabel('amplitude [m/s]')
            if rowindex == cnt-1:
                ax.set_xlabel('time relative to P arrival [s]')
            plt.tight_layout()
            plt.locator_params(axis='y', nbins=5)
            plt.locator_params(axis='x', nbins=5)

        savename =     '%s/singleplot_%s.png' %(plotpath,code)
        fig.savefig('%s' %(savename), dpi = 300)
        plt.close(fig)
        print >> sys.stdout, "Saving single station plot to ", savename
        figureLinks['---singleStation-link---']['%s' %code] = savename

    linksDict.update(figureLinks)

    return linksDict



##############################################################################
'''
inputreader
Return: eventParameter dictionary, laTex template list
'''

## Read in the event.xml file with the seiscomp3 logic
## Return: event paramaters dictionary
def readEventParameters(args):
    parameters = {}

    directory = args.inputfile
    fileName = '%s/event.xml' %directory


    ar = seiscomp3.IO.XMLArchive()
    if ar.open(fileName) == False:
        print >> sys.stderr, "Could not open %s" %fileName
        return False

    obj = ar.readObject()
    if obj is None:
        print >> sys.stderr, "Empty document"
        return False

    ep = seiscomp3.DataModel.EventParameters.Cast(obj)

    if ep is None:
        print >> sys.stderr, "Expected EventParameters, got %s." % obj.className()
        return False

    if ep.originCount() > 2:
        print >> sys.stderr, "unsupported event XML: %s" % fileName
        print >> sys.stderr, "expecting 2 origins but got %d" % ep.originCount()
        return False

    ## System
    parameters['---LOGO---'] = companyLogo

    ## Event
    distances = []
    event = ep.event(0)
    parameters['---EVENTID---'] = event.publicID()
    parameters['---AGENCYID---'] = event.creationInfo().agencyID().replace('_','\_').replace('#','\#')
    parameters['---AUTHOR---'] = event.creationInfo().author()
    # creationTime = event.creationInfo().creationTime()
    for i in xrange(event.eventDescriptionCount()):
        ed = event.eventDescription(i)
        if ed.type() == seiscomp3.DataModel.REGION_NAME:
            parameters['---REGION---'] = ed.text().replace('_','\_').replace('#','\#')
            break
        else:
            parameters['---REGION---'] = 'Not set'

    parameters['---FM-PREFID---'] = event.preferredFocalMechanismID().replace('_','\_').replace('#','\#')
    prefOrig = event.preferredOriginID()
    prefMag = event.preferredMagnitudeID()

    magnitude = seiscomp3.DataModel.Magnitude.Find(event.preferredMagnitudeID())
    if magnitude:
        parameters['---PREF-MAGNITUDE---'] = "%.2f" %magnitude.magnitude().value()
        parameters['---PREF-MAGNITUDETYPE---'] = magnitude.type()

    for i in [0,1]: ## its only valid if there are exactly 2 origins
        if prefOrig == ep.origin(i).publicID():
            origin = ep.origin(i)

            if origin.quality().associatedPhaseCount() == 0:
                print >> sys.stderr, "preferred origin %s has no phase picks" % origin.publicID()
                return False

            parameters['---EVORI-TIME---'] = origin.time().value()
            parameters['---EVORI-LATITUDE---'] = "%.3f" %origin.latitude().value()
            parameters['---EVORI-LONGITUDE---'] = "%.3f" %origin.longitude().value()
            parameters['---EVORI-DEPTH---'] = "%.2f" %origin.depth().value()
            parameters['---EVORI-RMS---'] = "%.3f" %origin.quality().standardError()
            parameters['---EVORI-METHODID---'] = origin.methodID().replace('_','\_').replace('#','\#')
            parameters['---EVORI-EARTHMODEL---'] = origin.earthModelID().replace('_','\_').replace('#','\#')

            pickcnt = origin.quality().associatedPhaseCount()

            parameters['---PICKS---'] = {}
            for ii in range(0,pickcnt):
                pickID = origin.arrival(ii).pickID()
                azim = origin.arrival(ii).azimuth()
                dist = origin.arrival(ii).distance()
                distances.append(dist)
                phase = origin.arrival(ii).phase()

                try:
                    polarity = origin.arrival(ii).polarity()
                except:
                    polarity = None

                try:
                    phaseRes = origin.arrival(ii).residuum()
                except:
                    phaseRes = 1000.
                if origin.arrival(ii).weight() == 1:
                    parameters['---PICKS---'][pickID] = {'azimuth': azim,
                                                 'distance': dist,
                                                 'phase': phase,
                                                 'polarity': polarity,
                                                 'residuum': phaseRes}

        else:
            centroid = ep.origin(i)
            parameters['---CENTROID-TIME---'] = centroid.time().value()
            parameters['---CENTROID-LATITUDE---'] = "%.3f" %centroid.latitude().value()
            parameters['---CENTROID-LONGITUDE---'] = "%.3f" %centroid.longitude().value()
            parameters['---CENTROID-DEPTH---'] = "%.2f" %centroid.depth().value()
            # parameters['---CENTROID-RMS---'] = "%.3f" %centroid.quality().standardError()
            parameters['---CENTROID-METHODID---'] = centroid.methodID()
            parameters['---CENTROID-EARTHMODEL---'] = centroid.earthModelID().replace('_','\_').replace('#','\#')
            parameters['---CENTROID-TYPE---'] = centroid.type()
            centroidMag = centroid.magnitude(0)
            parameters['---CENTROID-MAGNITUDE---'] = "%.2f" %centroidMag.magnitude().value()
            parameters['---CENTROID-MAGNITUDETYPE---'] = centroidMag.type()

    ## Focal Mechanism
    FM = ep.focalMechanism(0)
    parameters['---FM-ID---'] = FM.publicID().replace('_','\_').replace('#','\#')
    parameters['---FM-AUTHOR---'] = FM.creationInfo().author().replace('_','\_').replace('#','\#')
    parameters['---FM-AGENCYID---'] = FM.creationInfo().agencyID().replace('_','\_').replace('#','\#')
    parameters['---FM-TIME---'] = FM.creationInfo().creationTime()

    parameters['---FM-MISFIT---'] = "%.3f" %FM.misfit()
    parameters['---FM-AZGAP---'] = "%.2f" %FM.azimuthalGap()

    parameters['---FM-NP1S---'] = "%.2f" %FM.nodalPlanes().nodalPlane1().strike().value()
    parameters['---FM-NP1D---'] = "%.2f" %FM.nodalPlanes().nodalPlane1().dip().value()
    parameters['---FM-NP1R---'] = "%.2f" %FM.nodalPlanes().nodalPlane1().rake().value()

    parameters['---FM-NP2S---'] = "%.2f" %FM.nodalPlanes().nodalPlane2().strike().value()
    parameters['---FM-NP2D---'] = "%.2f" %FM.nodalPlanes().nodalPlane2().dip().value()
    parameters['---FM-NP2R---'] = "%.2f" %FM.nodalPlanes().nodalPlane2().rake().value()

    parameters['---FM-PAZIM---'] = FM.principalAxes().pAxis().azimuth().value()
    parameters['---FM-PPLUNGE---'] = FM.principalAxes().pAxis().plunge().value()
    parameters['---FM-TAZIM---'] = FM.principalAxes().tAxis().azimuth().value()
    parameters['---FM-TPLUNGE---'] = FM.principalAxes().tAxis().plunge().value()
    parameters['---FM-NAZIM---'] = FM.principalAxes().nAxis().azimuth().value()
    parameters['---FM-NPLUNGE---'] = FM.principalAxes().nAxis().plunge().value()

    ## Moment Tensor
    MT = FM.momentTensor(0)
    if MT.dataUsedCount() == 0:
        print  >> sys.stderr, 'Exit: Moment tensor has no reference to waveform data'
        return False
    parameters['---MT-DEVIATORIC---'] = "%.2f" %((1.0 - abs(float(MT.iso())))*100.)
    parameters['---MT-DC---'] = "%.2f" %(float(MT.doubleCouple())*100.)
    parameters['---MT-CLVD---'] = "%.2f" %(float(MT.clvd())*100.)
    try:
        parameters['---MT-ISO---'] = "%.2f" %(float(MT.iso())*100.)
    except:
        parameters['---MT-ISO---'] = "-"
    parameters['---MT-STATIONS---'] = MT.dataUsed(0).stationCount()
    parameters['---MT-COMPONENTS---'] = MT.dataUsed(0).componentCount()
    parameters['---MT-MOMENT---'] = "%.2g" %MT.scalarMoment().value()
    parameters['---MT-MRR---'] = "%.2g" %MT.tensor().Mrr().value()
    parameters['---MT-MTT---'] = "%.2g" %MT.tensor().Mtt().value()
    parameters['---MT-MPP---'] = "%.2g" %MT.tensor().Mpp().value()
    parameters['---MT-MRT---'] = "%.2g" %MT.tensor().Mrt().value()
    parameters['---MT-MRP---'] = "%.2g" %MT.tensor().Mrp().value()
    parameters['---MT-MTP---'] = "%.2g" %MT.tensor().Mtp().value()

    parameters['maxDist'] = max(distances)

    return parameters


## Read in the template laTex file
## Return: a list with all lines of the file
def readTemplate(filePath):
    filePath = os.path.expandvars(filePath)
    template = []
    with open(filePath, 'r') as file:
        for line in file:
            template.append(line)
    return template



###############################################################################
'''
contentgenerator
Return: output laTex file content

There are no empty lines allowed in template.tex!
'''

def generateTitle(filename, parameters, template):
    outputfile = open(filename, 'a')
    for line in template[:]:
        newline = line.rstrip()
        if not newline:
            continue
        for f_key, f_value in parameters.items():
            if f_key in newline:
                if f_key == '---LOGO---':
                    outputfile.write('\\begin{figure}[!th]\n')
                    outputfile.write('\centering\n')
                    outputfile.write('\includegraphics[width=1\\textwidth]{%s}\n'%f_value)
                    outputfile.write('\end{figure}\n')
                    newline = newline.replace(f_key, '')
                else:
                    newline = newline.replace(f_key, f_value)
        outputfile.write(newline + "\n")
        template.remove(line)
        if line[0:4] == "%<Ti":
            outputfile.close()
            return template


def generateEQSummary(filename, parameters, eqparameters, template):
    z = parameters.copy()
    z.update(eqparameters)
    outputfile = open(filename, 'a')
    for line in template[:]:
        newline = line.rstrip()
        for f_key, f_value in z.items():
            if f_key in newline:
                newline = newline.replace(f_key, str(f_value))
                if str(f_value) == "not available":
                    newline = "%"+newline
        outputfile.write(newline + "\n")
        template.remove(line)
        if line[0:4] == "%<EQ":
            outputfile.close()
            return template


def generateDepthSearchPlot(filename, parameters, eqparameters, template):
    z = parameters.copy()
    z.update(eqparameters)
    outputfile = open(filename, 'a')
    for line in template[:]:
        newline = line.rstrip()
        for f_key, f_value in z.items():
            if f_key in newline:
                if f_value == 'not available':
                    outputfile.write('Depth search is not available')
                else:
                    outputfile.write('\\begin{figure}[!bh]\n')
                    outputfile.write('\centering\n')
                    outputfile.write('\includegraphics[width=1\\textwidth]{%s}\n'%f_value)
                    outputfile.write('\caption{Centroid depth search. Quality: product of the relative station count and the fit.}\n')
                    outputfile.write('\end{figure}\n')
                newline = newline.replace(f_key, '')
        outputfile.write(newline + "\n")
        template.remove(line)
        if line[0:4] == "%<DS":
            outputfile.close()
            return template


def generateDistPlot(filename, parameters, eqparameters, template):
    z = parameters.copy()
    z.update(eqparameters)
    outputfile = open(filename, 'a')
    for line in template[:]:
        newline = line.rstrip()
        for f_key, f_value in z.items():
            if f_key in newline:
                if f_key == '---distplot-link---':
                    for phasetyp, link in z[f_key].items():
                        outputfile.write('\\begin{figure}[!bh]\n')
                        outputfile.write('\centering\n')
                        outputfile.write('\includegraphics[width=1\\textwidth]{%s}\n'%link)
                        outputfile.write('\caption{Waveform of %ss - %s component - shown as used for inversion.}\n' %(phasetyp.split(' ')[0],phasetyp.split(' ')[1]))
                        outputfile.write('\end{figure}\n')
                        newline = newline.replace(f_key, '')
                else:
                    newline = newline.replace(f_key, f_value)
        outputfile.write(newline + "\n")
        template.remove(line)
        if line[0:4] == "%<DP":
            outputfile.close()
            return template


def generateSingleStationPlot(filename, parameters, eqparameters, template):
    z = parameters.copy()
    z.update(eqparameters)
    outputfile = open(filename, 'a')
    for line in template[:]:
        newline = line.rstrip()
        for f_key, f_value in z.items():
            if f_key in newline:
                if f_key == '---singleStation-link---':
                    for station, link in z[f_key].items():
                        net = station.rsplit('_')[0]
                        sta = station.rsplit('_')[1]
                        outputfile.write('\\begin{figure}[htbp]\n')
                        outputfile.write('\centering\n')
                        outputfile.write('\includegraphics[width=1\\textwidth]{%s}\n'%link)
                        outputfile.write('\caption{Waveforms for %s.%s at given epicentral distance shown as used for inversion.}\n' %(net,sta))
                        outputfile.write('\end{figure}\n')
                        outputfile.write('\\FloatBarrier\n')

                        newline = newline.replace(f_key, '')
                else:
                    newline = newline.replace(f_key, f_value)
        outputfile.write(newline + "\n")
        template.remove(line)
        if line[0:4] == "%<SS":
            outputfile.close()
            return template


def generateBody(filename, template):
    outputfile = open(filename, 'a')
    for line in template[:]:
        newline = line.rstrip()
        if not newline:
            continue
        outputfile.write(newline + "\n")
        template.remove(line)
        break
    return template


################################################################################
'''
Main function
Generates with the event.xml, depthsearch.tbl and the waveform data a laTex report
'''

def main(argv):

    defaultoutputfile = '/tmp/'
    defaultTemplate = '$SEISCOMP_ROOT/share/mtv/tools/mtv-plot-extended-log.tex'

    parser = argparse.ArgumentParser(description=
        '''
        Description: Create a report from moment tensor inversion
        Examples: ~/seiscomp3/share/mtv/tools/mtv-plot-extended-log.py ~/.seiscomp3/log/MT/ext/FocalMechanism_20180529131208.262757.33692
        Default:
        ''',
         formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("inputfile", help="echo the string you use here")
    args = parser.parse_args()

    outdir = os.path.join(args.inputfile, "report")
    outDoc = os.path.join(outdir,"output.tex")

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    if os.path.exists(outDoc):
        os.remove(outDoc)


    plotpath = '%s/media' %outdir
    if not os.path.exists(plotpath):
        os.makedirs(plotpath)

    template = readTemplate(defaultTemplate)

    EQParameters = readEventParameters(args)
    if not EQParameters:
        return False

    figureLinks = {}
    try:
        figureLinks = generateMap(args, figureLinks, EQParameters, plotpath)
    except:
        print "error in generating map plot"
        figureLinks['---map-link---'] = 'not available'
        figureLinks.update(figureLinks)
    try:
        figureLinks = plot_mt(args, figureLinks, EQParameters, plotpath)
    except:
        print "error in generating MT plot"
        figureLinks['---mtsolution-link---'] = 'not available'
        figureLinks.update(figureLinks)
#    try:
    figureLinks = depthSearch(args, figureLinks, EQParameters, plotpath)

#    except:
#        print "error in generating depth plot"
#        figureLinks['---depthsearch-link---'] = 'not available'
#        figureLinks.update(figureLinks)
    try:
        figureLinks = distplot(args, figureLinks, plotpath, EQParameters)
    except:
        print "error in generating seismogram-distance plots"
        figureLinks['---distplot-link---'] = 'not available'
        figureLinks.update(figureLinks)

    try:
        figureLinks = singleStationPlot(args, figureLinks, plotpath)
    except:
        print "error in generating single-station plots"
        figureLinks['---singleStation-link---'] = 'not available'
        figureLinks.update(figureLinks)

    EQParameters['---TIME---'] = str(datetime.datetime.utcnow())

    while len(template[:])>0:
        line=template[0]
        if line[0:4]=="%>Ti":
            template=generateTitle(outDoc, EQParameters, template)
        elif line[0:4]=="%>EQ":
            template=generateEQSummary(outDoc, figureLinks, EQParameters, template)
        elif line[0:4] == "%>DS":
            template=generateDepthSearchPlot(outDoc, figureLinks, EQParameters, template)
        elif line[0:4] == "%>DP":
            template=generateDistPlot(outDoc, figureLinks, EQParameters, template)
        elif line[0:4] == "%>SS":
            template=generateSingleStationPlot(outDoc, figureLinks, EQParameters, template)
        else:
            template=generateBody(outDoc, template)
            if template == ['\n']:
                break

    cmdWhichXelatex = 'which xelatex'
    process = subprocess.Popen(cmdWhichXelatex.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    if not output:
        print  >> sys.stderr, 'xelatex not found - checkout your installation!'
        return False

    xelatexpath = output.rsplit()[0]

    cmdCreatePdf = "%s -interaction nonstopmode -output-directory=%s/ %s " %(xelatexpath, outdir, outDoc)
    print >> sys.stdout, "executing: ", cmdCreatePdf
    process = subprocess.Popen(cmdCreatePdf.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if error != "none":
        pdfFile=os.path.join(outdir,"output.pdf")
        print >> sys.stdout, "Created PDF file: ",pdfFile
    # show pdf:
        webbrowser.open(pdfFile)
    else:
        print >> sys.stdout, output
        print >> sys.stdout, error
        print >> sys.stdout, "\nSome errors have occurred during LaTeX compilation"


if __name__ == '__main__':
   sys.exit(main(sys.argv))
