<?xml version="1.0" encoding="UTF-8"?>
<seiscomp>
	<plugin name="mapmultigrid">
		<extends>global</extends>
		<configuration>
			<group name="map">
				<group name="multigrid">
					<description>Configuration of the mapmultigrid plugin allowing
						 to display multiple grids on maps.
					 </description>
					<parameter name="profiles" type="list:string">
						<description>
						Registration of profiles
						</description>
					</parameter>
					<parameter name="mode" type="string" default="poly">
						<description>
						Render mode of the mapmultigrid plugin. Modes are: grid, line, poly.
						</description>
					</parameter>

					<group name="profiles">
						<description>Add one profile per grid file to show on maps.</description>
						<struct type="Grid profile" link="profiles">
							<parameter type="string" name="watch">
								<description>
								Absolute filename to track for changes with inotify.
								</description>
							</parameter>
							<parameter type="string" name="title" default="">
								<description>
								The title of the legend.
								</description>
							</parameter>
							<parameter type="string" name="legendArea" default="bottomright">
								<description>
								Sets the location of grid legend. Use either:
								topleft, topright, bottomright or bottomleft.
								</description>
							</parameter>
							<parameter type="boolean" name="discrete" default="true">
								<description>
								Use a discrete gradient is for rendering in grid mode.
								</description>
							</parameter>
							<parameter type="boolean" name="visible" default="true">
								<description>
								Make this item visible at startup.
								</description>
							</parameter>
							<parameter type="string" name="composition" default="src-over">
								<description>
								Sets the composition mode. Specifies how the pixels
								of the grid(the source) are merged with the pixels
								of the map(the destination).

								Available:

								src-over, dst-over, clear, src, dst, src-in, dst-in,
								src-out, dst-out, src-atop, dst-atop, xor, plus,
								muliply, screen, darken,
								lighten, color-dodge, color-burn, hard-light,
								soft-light, difference, exclusion, src-or-dst,
								src-and-dst, src-xor-dst, not-src-and-not-dst,
								not-src-or-not-dst, not-src-xor-dst, not-src,
								not-src-and-dst and src-and-not-dst

								See the QT documentation: https://doc.qt.io/archives/qt-4.8/qpainter.html#CompositionMode-enum
								for the modes.
								</description>
							</parameter>
							<parameter type="boolean" name="renderBackToFront" default="false">
								<description>
								If true, render polygons in inverse value order,
								highest value first and lowest value last.
								</description>
							</parameter>
							<parameter type="gradient" name="stops">
								<description>
								Gradient used for rendering the grid.

								Example (value:color:style):

								30.00:ff00fa:"major",60.00:f25ffb:"minor"

								where minor and major refer to a style profile.
								</description>
							</parameter>
							<parameter type="string" name="style">
								<description>
								Link to style profile.
								</description>
							</parameter>
						</struct>
					</group>
					<group name="styles">
						<description>
							Add parameters defining the style profiles used
							by the multi grid profiles.
						</description>
					<struct type="Style profile">
						<group name ="brush">
							<description>
							Brushes are used to fill a polygon. They have two attributes: color and pattern. Color followes the described
							SeisComP3 color definition and patterns are: solid, dense1, dense2, dense3, dense4,
							dense5, dense6, dense7, nobrush, horizontal, vertical, cross, bdiag, fdiag,
							diagcross.
							</description>
							<parameter type="string" name="color" default="000000FF">
								<description>
								The brush color.
								</description>
							</parameter>
							<parameter type="string" name="pattern" default="solid">
								<description>
								The brush pattern
								</description>
							</parameter>
						</group>
						<group name ="pen">
							<description>
							Pens are used to draw lines. They have three attributes: color, style and width.
							Color follows the described SeisComP3 color definition, width is a double and the styles are:
							nopen, solidline, dotline, dashline, dashdotline, dashdotdotline.
							</description>
							<parameter type="string" name="color" default="000000FF">
								<description>
								The pen color in hexadecimal representation.
								</description>
							</parameter>
							<parameter type="string" name="style" default="solidline">
								<description>
								The pen style. Available:
								nopen, solidline, dotline, dashline, dashdotline, dashdotdotline
								</description>
							</parameter>
							<parameter type="double" name="width" default="1.0" unit="px">
								<description>
								The pen width. A double.
								</description>
							</parameter>
						</group>
					</struct>
					</group>
				</group>
			</group>
		</configuration>
	</plugin>
</seiscomp>
