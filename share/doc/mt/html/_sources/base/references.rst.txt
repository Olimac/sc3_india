References
**********

scmtv and scautomt have been operational in real time in many data centres world wide.
The results have been used, demonstrated, promoted and discussed with scientists
and the SeisComP3 community in scientific publications and at international science conferences, e.g.:

#. W. Hanka, J. Saul, B. Weber, J. Becker, P. Harjadi, Fauzi, and GITEWS Seismology Group,
   Real-time earthquake monitoring for tsunami warning in the Indian Ocean and beyond,
   2010, Nat. Haz. Earth Sys. Sci., 10 (2611-2622),
   doi: `10.5194/nhess-10-2611-2010 <https://www.nat-hazards-earth-syst-sci.net/10/2611/2010/nhess-10-2611-2010.pdf>`_
#. J. Saul, J. Becker, W. Hanka, Global moment tensor computation at GFZ Potsdam, 2011.
   AGU Fall Meeting, Sn Francisco, USA, abstract ID. `S51A-2202 <https://ui.adsabs.harvard.edu/abs/2011AGUFM.S51A2202S>`_
#. D. Roessler, S. Al-Harthy, B. Weber, B. Alrumhi, and the team of gempa, Monitoring
   of earthquakes and tsunamis at the National Tsunami Warning Center of DGMET, Oman, 2018,
   10th Gulf Seismic Forum, Muscat, Oman
