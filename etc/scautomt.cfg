# The greens function archive URL.
gfaUrl = sc3gf1d:///home/data/greensfunctions

# The greens function model to use.
automt.gfModel = gemini-prem



# The greens function archive URL
#gfaUrl = helmberger:///path/to/archive

# The greens function model to use.
#automt.gfModel = supported-model

# The maximum age of an event to be
# processed.
automt.events.maxAge = 3600

# Defines the goodness of fit function to be used. Allowed values are:
# - internal
# - varred
automt.GOF = internal

# Maximum number of parallel processes.
# Currently two threads are used per event:
#  - acquisition thread
#  - processing thread
# whereas the acquisition thread is low
# profile. The heavy work in terms of
# CPU and RAM usage is done in the
# processing thread.
automt.maximumProcesses = 4

# Minimum event magnitude to trigger
# the MT calculation
automt.trigger.minimumMagnitude = 5.5

# Minimum used station count of an
# event to trigger the MT calculation
automt.trigger.minimumStations = 20

# The maximum allowed station distance in degrees.
automt.maximumDistance = 50.0

# How many seconds of data to use before the
# signal to stabilize deconvolution and so on.
automt.data.leftNoiseLength = 600

# How many seconds of data after the signal
# to use and to fetch.
automt.data.rightNoiseLength = 0

# Safety margin around the signal to shift the traces
# at different depth and distances.
automt.data.safetyMargin = 120


# The initial depth search grid. A list of tokens
# in format [depth]:[km]. The first token creates
# a grid from 0 to [depth] with spacing [km].
# Any subsequent token creates a grid from the previous
# token with spacing [km]. If the depths is left out
# then it is valid until the maximum depth of the
# greens functions.
automt.depthSearchGrid = 100:20, 200:30, 400:50, 100

# The depth search intervals. All intervals are processed
# subsequently. The predecessor and successor of the best
# fitting depth are used as a new interval for the next
# search with a finer increment.
automt.depthFineSearchIncrements = 50, 10, 5, 1


# Global phase settings
automt.settings.minSNR.body = 1
automt.settings.minSNR.surface = 1
automt.settings.minSNR.mantle = 1
automt.settings.minSNR.w-phase = 1

automt.settings.maxShift.body = 10
automt.settings.maxShift.surface = 10
automt.settings.maxShift.mantle = 10
automt.settings.maxShift.w-phase = 10

# Default component weights: vertical, radial and tangential
automt.settings.wZ = 1.0
automt.settings.wR = 0.25
automt.settings.wT = 0.5

# Some default magnitude profiles
automt.profiles = wphase0, wphase1, wphase2,\
                  wphase3, wphase4

# Magnitude ranges are disabled for the moment to
# not use it automatically
automt.profiles.wphase0.minItemFit = 75
automt.profiles.wphase0.maxShift = 25
automt.profiles.wphase0.periods.w-phase = 50-150
automt.profiles.wphase0.magnitudes = 5.5;6.5

automt.profiles.wphase1.minItemFit = 75
automt.profiles.wphase1.maxShift = 45
automt.profiles.wphase1.periods.w-phase = 100-500
automt.profiles.wphase1.magnitudes = 6.5;7.0

automt.profiles.wphase2.minItemFit = 75
automt.profiles.wphase2.maxShift = 45
automt.profiles.wphase2.periods.w-phase = 100-600
automt.profiles.wphase2.magnitudes = 7.0;7.5

automt.profiles.wphase3.minItemFit = 75
automt.profiles.wphase3.maxShift = 60
automt.profiles.wphase3.periods.w-phase = 200-600
automt.profiles.wphase3.magnitudes = 7.5;8.0

automt.profiles.wphase4.minItemFit = 75
automt.profiles.wphase4.maxShift = 60
automt.profiles.wphase4.periods.w-phase = 200-1000
automt.profiles.wphase4.magnitudes = 8.0;INF


# The minimum station count for a valid MT solution.
automt.minimumStationCount = 6

# The minimum fit of a station of a final solution.
# Stations are removed below this threshold unless the
# minimum station count has been reached.
automt.minimumFinalStationFit = 0.3

# The minimum final fit for a MT solution to be
# valid.
automt.minimumFinalFit = 0.3

# When in realtime mode to start a new intermediate
# processing step it required to get the data of
# X*current more stations within a certain timespan
# (see below) whereas X is this parameter
# and current the currently number of stations with
# complete data.
automt.relativIncrementalStationCount = 0.5

# Timespan in seconds to calculate the expected
# final station count for. It is estimated based
# on the current time and the end of the requested
# time window of a data stream.
automt.expectedDataTimeSpan = 120

# The maximum data delay in realtime mode to
# exlcude stations from the above calculation.
# The delay is now - requested_data_endtime.
automt.maximumDataDelay = 120

# Set station weight according to distance:
#  w = dist/min(dists)
# If false all stations are equally weighted.
automt.weightStationsByDistance = false

# Define surface wave group velocities to pick
# the surface wave onset.
automt.velocity.rayleigh = 4.2
automt.velocity.love = 5.0

# Enables or disables the calculation of the CMT.
# The CMT calculation can take some time (depending
# on the number of stations and distances) and
# uses a simple gradient grid search.
automt.centroid.enabled = false

# The initial centroid grid search cell size in km.
automt.centroid.startCellSize = 40.0

# The final centroid grid search cell size. The final
# CMT solution is defined after subdividing the grid
# reaches a cell size equal or less than this value.
automt.centroid.finalCellSize = 1.0

# Defines the region of the event to trigger MT processing
# Events outside this region are ignored.
# Format: minlat, minlon, maxlat, maxlon
automt.region = -90, -180, 90, 180
