#!/bin/bash
yum install python-matplotlib python-tk texlive-xetex texlive-latex-extra texlive-collection-xetex

echo "user name for pip install (sysop):"
read user

if  [ -z "$user" ]; then user=sysop; fi

# install Python packages using pip
echo "installing packages for user " $user
su - $user -c 'pip install --user mplstereonet'
