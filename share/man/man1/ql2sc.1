.\" Man page generated from reStructuredText.
.
.TH "QL2SC" "1" "Jan 31, 2020" "2020.030" "SeisComP3"
.SH NAME
ql2sc \- SeisComP3 Documentation
.
.nr rst2man-indent-level 0
.
.de1 rstReportMargin
\\$1 \\n[an-margin]
level \\n[rst2man-indent-level]
level margin: \\n[rst2man-indent\\n[rst2man-indent-level]]
-
\\n[rst2man-indent0]
\\n[rst2man-indent1]
\\n[rst2man-indent2]
..
.de1 INDENT
.\" .rstReportMargin pre:
. RS \\$1
. nr rst2man-indent\\n[rst2man-indent-level] \\n[an-margin]
. nr rst2man-indent-level +1
.\" .rstReportMargin post:
..
.de UNINDENT
. RE
.\" indent \\n[an-margin]
.\" old: \\n[rst2man-indent\\n[rst2man-indent-level]]
.nr rst2man-indent-level -1
.\" new: \\n[rst2man-indent\\n[rst2man-indent-level]]
.in \\n[rst2man-indent\\n[rst2man-indent-level]]u
..
.sp
\fBQuakeLink (gempa GmbH) to SeisComP3 event parameter exchange\fP
.SH DESCRIPTION
.sp
ql2sc manages the import of SC3 objects from one or several QuakeLink servers
into a SeisComP3 system in real time. Like scimex but contrary to
scimport the exchange of the SC3 objects is event based. This means no
messages will be exchanged until the exporting system has produced an event.
.sp
The user may control at various levels which information to import. Whenever
possible server\-side filters should be preferred to reduce both the network
bandwidth consumption as well as the CPU and memory utilization on the local
machine.
.SH SERVER-SIDE EVENT FILTER
.sp
QuakeLink provides a filter syntax similar to SQL\-WHERE clauses which may be
used to filter interesting events on the server side:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
clause    := condition[ AND|OR [(]clause[)]]
condition := MAG|DEPTH|LAT|LON|PHASES|OTIME|UPDATED [op float|time]|[IS [NOT] NULL]
op        := =|&gt;|&gt;=|&lt;|&lt;=|eq|gt|ge|lt|ge
time      := %Y,%m,%d[,%H,%M,%S,%f]
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
E.g. the following filter string would select only those events with a minimum
magnitude of 6, detected by at least 10 stations and which are shallower than
100km:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
MAG >= 6.0 AND PHASES >= 10 AND DEPTH < 100
.ft P
.fi
.UNINDENT
.UNINDENT
.SH SERVER-SIDE OBJECT FILTER
.sp
QuakeLink provides a coarse object filter for the most relevant SC3 objects:
.TS
center;
|l|l|.
_
T{
Option
T}	T{
Impact
T}
_
T{
picks
T}	T{
include picks
T}
_
T{
amplitudes
T}	T{
include amplitudes
T}
_
T{
arrivals
T}	T{
include origin arrivals
T}
_
T{
staMags
T}	T{
include origin station magnitudes
T}
_
T{
staMts
T}	T{
include moment tensor station contributions and phase settings
T}
_
T{
preferred
T}	T{
include only preferred origin and magnitude information
T}
_
.TE
.SH LOCAL OBJECT FILTER AND ROUTING
.sp
Subsequent to the server\-side filters a routing table defines which objects to
import and to which message group to send them. Depending on the SC3 modules
listening to the specified message groups an object may be further processed.
Typically no modules (other than scmaster) is connected to the
\fBIMPORT_GROUP\fP so that objects sent to this group are just stored to the
database. If an object should be discarded the special group identifier \fBNULL\fP
may be used.
.sp
The routing table is defined as a comma\-separated list of
\fBobject name:group name\fP pairs. Also the routing rules are inherited
recursively within the SC3 object tree. If no explicit rule exists for an object
the routing of its parent is evaluated up to the \fBEventParameters\fP root node.
.SS Examples
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
EventParameters:IMPORT_GROUP
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Imports everything
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
EventParameters:IMPORT_GROUP,Comment:NULL
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Imports everything except comments
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
Origin:LOCATION,StationMagnitude:MAGNITUDE,Magnitude:MAGNITUDE
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Sends origins and it\(aqs children arrival, origin uncertainty to the \fBLOCATION\fP
group but the magnitude children to the \fBMAGNITUDE\fP group. Skips picks,
amplitudes, focal mechanisms and events.
.SS Default routing table
.sp
The default use case of ql2sc is to import earthquake solutions from other data
centers or in\-house redundant SeisComP3 systems. The intention is not to
reprocess the solution but to add them to the local catalog.
.sp
By default we route:
.INDENT 0.0
.IP \(bu 2
Picks and Amplitudes to the \fBIMPORT_GROUP\fP group to prevent processing by
the local locator and amplitude processor
.IP \(bu 2
Origins (including its StationMagnitude and Magnitude children) to the
\fBLOCATION\fP to allow event association.
.IP \(bu 2
FocalMechanisms to the \fBFOCMECH\fP group to trigger processing by specialized
applications, e.g. graphical user interfaces for strong motion analysis or
tsunami risk assessment.
.UNINDENT
.sp
We don\(aqt route events at all. With the help of scevent locations are
either associated to existing events or will create new events with local
settings.
.sp
We don\(aqt route StationMagnitudes and Magnitude to the \fBMAGNITDUE\fP group
because scmag subscribes to \fBLOCATION\fP and \fBMAGNITUDE\fP\&. Separated
groups might lead to duplicated magnitude types in case a manual magnitude
solution is imported. In this case the foreign Origin with its Magnitudes would
be split into at least two messages, the first one containing the Origin, the
second one the Magnitude. The Origin message immediately triggers magnitude
calculation, potentially for a magnitude type which is received with the second
message.
.sp
The default routing table is set to:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
Pick:IMPORT_GROUP,Amplitude:IMPORT_GROUP,FocalMechanism:FOCMECH,Origin:LOCATION
.ft P
.fi
.UNINDENT
.UNINDENT
.SH AGENCY LIST FILTER
.sp
In addition to the local object filter the user may choose to accept only those
objects originating from a set of trusted agencies. If at least one agency is
defined in the \fBprocessing.whitelist.agencies\fP or
\fBprocessing.blacklist.agencies\fP configuration option, then the
\fBcreationInfo.agencyID\fP of amplitudes, arrivals, comments, events, focal
mechanisms, magnitudes, moment tensors, origins, picks and station magnitudes is
evaluated. Objects with unmatched or unset agency information are filtered out.
If objects with unset agency id should match then empty string \fB""\fP has to be
added to the white list.
.sp
The agency filter is applied on remote as well as local objects. In this way
remote objects may be excluded from import and local objects my be protected
from overriding or removing. Also the filter is applied recursively. If parent
object (e.g. an origin) is filtered out all of its children (e.g. magnitudes)
are also skipped even if they carry a different agency id.
.sp
\fBNOTE:\fP
.INDENT 0.0
.INDENT 3.5
The agency white list filter might be essential to avoid circular event
updates between cross\-connected SC3 systems.
.UNINDENT
.UNINDENT
.SH WORKFLOW
.sp
Each event update received from a QuakeLink host is parsed and analyzed for
differences to the local database. The comparison starts at the level of the
top\-level elements in the following order: picks, amplitudes, origins, focal
mechanisms, events.
.sp
For each top\-level element the object tree is traversed in a depth\-first search
order. Objects on the same level are processed in the order of their appearance.
The differences are collected as a list of notifier objects with the following
operation types:
.TS
center;
|l|l|.
_
T{
Type
T}	T{
Description
T}
_
T{
ADD
T}	T{
The object does not exist locally
T}
_
T{
UPDATE
T}	T{
The object does exist locally but differs from the remote one
T}
_
T{
REMOVE
T}	T{
The object exist locally but not remotely
T}
_
.TE
.sp
The \fBADD\fP and \fBREMOVE\fP operation always generates notifies of the same type
for all children of the current object. \fBADD\fP notifiers are collected top\-down,
\fBREMOVE\fP notifiers are collected bottom\-up.
.sp
Because the order of child objects is arbitrary, e.g. the arrivals of an origin,
each object on the remote side has to be found in the set of local objects. For
public objects (e.g. origins, magnitudes, magnitudes..), the \fBpublicID\fP property
is used for comparison. All other objects are compared by looking at their index
properties. For e.g. arrivals this is the \fBpickID\fP property, for comments the
\fBid\fP property.
.sp
Ones all notifiers are collected they are send to the local messaging system.
For performance reasons and because of the processing logic of listening SC3
modules ql2sc tries to batch as many notifiers as possible into one notifier
message. A separate notifier message is created if the target message group
changes between successive notifiers or if the configurable batchSize
limit is reached.
.sp
\fBNOTE:\fP
.INDENT 0.0
.INDENT 3.5
Care must be taken when configuring the \fBbatchSize\fP limit. If the value
is to big the overall message size limit (default: 1MB) may be exceeded
resulting in an undeliverable message. On the other hand a much to small
value will create unwanted results in the SC3 processing chain. If for
instance picks are routed to the \fBPICK\fP group and the pick set is split
into several notifier messages the local scautoloc might create
locations based on an incomplete dataset.
.UNINDENT
.UNINDENT
.SH EVENT ATTRIBUTES
.sp
It might be desirable to synchronize event attributes set at the soure with
the local system. In particular the event type, the type uncertainty, event
descriptions and comments might be of interest. Because it is not advisable
to route events and let scevent associate imported origins it can
happen that the imported event id is different from the event id of the local
system. The input host configuration parameter \fBsyncEventAttributes\fP
controls that behaviour. It is set to true by default which means that imported
event attributes are going to be imported as well. ql2sc does not update
directly the attributes but commandates scevent in as many cases as possible
to do so. To find the matching local event it takes the first occurrence which
has associated the currently imported preferred origin.
.SS Limitations
.sp
There are limitations to this process to avoid infinite loops when cross
connecting two systems. Prior to sending the commands to scevent to change a
particular attribute ql2sc checks if that attribute has been set already by
another module (via JournalEntry database table). If not then ql2sc is allowed
to request an attribute change otherwise not. To illustrate the issue take the
following example:
.sp
scolv connected to system \fBA\fP changes the event type to \(aqearthquake\(aq. ql2sc
of system \fBB\fP checks if the event type of the local event has been changed
already which is not the case and it requests that change. System \fBA\fP
changes the event type again to \(aqunset\(aq. ql2sc of system \fBB\fP notices that
someone has already changed the event type and it was ql2sc itself. It requests
again a change.
.sp
scolv connected to system \fBB\fP changes the event type to \(aqearthquake\(aq again.
ql2sc of system \fBA\fP notices that \fBscolv@A\fP has already changed the
event type and ignores the request.
.sp
That simple case would not create an infinite loop even if \fBql2sc@A\fP would
accept the last change. The situation changes immediately if two subsequent
attribute changes are being received by \fBql2sc@B\fP while both of them are
already applied on system \fBA\fP\&. \fBql2sc@B\fP would "restore" the old state due
to the first received update and then apply the "final" state due to the
second update. Each update triggers again an update at system \fBA\fP and the
states start flapping. Without the described check there wouldn\(aqt be a well
defined exit condition.
.SH CAVEATS
.sp
Specific combinations of remote and local object filters may result in the loss
of data. If for instance origins are imported from system \fBA\fP to \fBB\fP and
additional magnitudes for the received origins are calculated on \fBB\fP care must
be taken. Without protection a new event update containing the same origin will
\fBREMOVE\fP all newly calculated magnitudes on \fBB\fP since they are not included
in the magnitude set sent by \fBA\fP\&.
.sp
To avoid losing these local magnitudes one may decide to block magnitudes from
import by routing them to \fBNULL\fP\&. If magnitudes from \fBA\fP and from \fBB\fP
should be available an \fI\%agency filter\fP may be defined. Make
sure \fBA\fP and \fBB\fP uses distinct agency IDs and add the agency ID of \fBB\fP to
\fBprocessing.blacklist.agencies\fP\&.
.SH CONFIGURATION
.nf
\fBetc/defaults/global.cfg\fP
\fBetc/defaults/ql2sc.cfg\fP
\fBetc/global.cfg\fP
\fBetc/ql2sc.cfg\fP
\fB~/.seiscomp3/global.cfg\fP
\fB~/.seiscomp3/ql2sc.cfg\fP
.fi
.sp
.sp
ql2sc inherits global options\&.
.INDENT 0.0
.TP
.B backLog
Type: \fIint\fP
.sp
Unit: \fIs\fP
.sp
Number of seconds to fetch missed updates on start up.
Default is \fB1800\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B cacheSize
Type: \fIint\fP
.sp
Number of public objects to cache.
Default is \fB5000\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B batchSize
Type: \fIint\fP
.sp
Maximum number of notifiers to batch in one message. If set
to 0 no size limit is enforced. Make sure to not hit the
overall message size limited of 16MiB which is enforced by
the messaging system.
Default is \fB2000\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B eventAssociationTimeout
Type: \fIint\fP
.sp
Unit: \fIs\fP
.sp
If event synchronisation is enabled and an incoming origin
is not yet associated with an event on the target machine
then this timeout defines the maximum number of seconds to
wait for an association.
Default is \fB10\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B hosts
Type: \fIlist:string\fP
.sp
Registration of the host profiles defining the connection
parameters to the QuakeLink hosts.
.UNINDENT
.sp
\fBNOTE:\fP
.INDENT 0.0
.INDENT 3.5
\fBhost.*\fP
\fIDefinition of host profiles. For each host profile a connection\fP
\fIto one QuakeLink server can established. The profiles must be registered\fP
\fIin \(aqhosts\(aq to apply them.\fP
.UNINDENT
.UNINDENT
.sp
\fBNOTE:\fP
.INDENT 0.0
.INDENT 3.5
\fBhost.$name.*\fP
\fIProvide the connection parameters to one QuakeLink server.\fP
$name is a placeholder for the name to be used and needs to be added to \fI\%hosts\fP to become active.
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
hosts = a,b
host.a.value1 = ...
host.b.value1 = ...
# c is not active because it has not been added
# to the list of hosts
host.c.value1 = ...
.ft P
.fi
.UNINDENT
.UNINDENT
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.url
Type: \fIstring\fP
.sp
URL of the QuakeLink service, the scheme \(aqqls\(aq enables SSL.
.sp
Format: [ql[s]://][user:pwd@][host][:port].
.sp
If set to an empty string the application will run without any QuakeLink connection attempt.
Default is \fBql://localhost:18010\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.gzip
Type: \fIboolean\fP
.sp
Enables/disables GZip (GNU zip) compression.
Default is \fBfalse\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.native
Type: \fIboolean\fP
.sp
Request native data instead of XML format.
Native data export may be disabled on some hosts.
Default is \fBfalse\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.syncEventAttributes
Type: \fIboolean\fP
.sp
Tries to update the event attributes of the target event
with the attributes of the source event which includes
event type and event certainty. It will not import
events but tries to find the associated event of the
input preferred origin at the target system and will
update the event attributes via journaling.
Default is \fBtrue\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.keepAlive
Type: \fIboolean\fP
.sp
Request server to send keep alive message every 30s to
prevent connection reset by firewalls on long idle
periods. If activated the client will reset the
connection if no alive message is received within 60s.
Default is \fBfalse\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.filter
Type: \fIstring\fP
.sp
SQL like WHERE clause to filter the result set.
.sp
clause    := condition[ AND|OR [(]clause[)]] __
condition := MAG|DEPTH|LAT|LON|PHASES|OTIME|UPDATED [op float|time]|[IS [NOT] NULL] __
op        := =|>|>=|<|<=|eq|gt|ge|lt|ge __
time      := %Y,%m,%d[,%H,%M,%S,%f]
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.routingTable
Type: \fIlist:string\fP
.sp
Maps datamodel class names to messaging groups. For unmapped objects
the mapping of their parent objects is evaluated recursively. Objects
may be excluded by mapping them to \(aqNULL\(aq.
Default is \fBPick:IMPORT_GROUP,Amplitude:IMPORT_GROUP,FocalMechanism:FOCMECH,Origin:LOCATION\fP\&.
.UNINDENT
.sp
\fBNOTE:\fP
.INDENT 0.0
.INDENT 3.5
\fBhost.$name.data.*\fP
\fISpecify the XML components to fetch.\fP
\fINote: These options are not used if \(aqnative\(aq data is requested.\fP
.UNINDENT
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.data.picks
Type: \fIboolean\fP
.sp
Include picks
Default is \fBtrue\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.data.amplitudes
Type: \fIboolean\fP
.sp
Include amplitudes
Default is \fBtrue\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.data.arrivals
Type: \fIboolean\fP
.sp
Include origin arrivals
Default is \fBtrue\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.data.staMags
Type: \fIboolean\fP
.sp
Include origin station magnitudes
Default is \fBtrue\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.data.staMts
Type: \fIboolean\fP
.sp
Include moment tensor station contributions and phase settings
Default is \fBtrue\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B host.$name.data.preferred
Type: \fIboolean\fP
.sp
Include only preferred origin and magnitude information
Default is \fBtrue\fP\&.
.UNINDENT
.SH COMMAND-LINE
.SS Generic
.INDENT 0.0
.TP
.B \-h, \-\-help
show help message.
.UNINDENT
.INDENT 0.0
.TP
.B \-V, \-\-version
show version information
.UNINDENT
.INDENT 0.0
.TP
.B \-\-config\-file arg
Use alternative configuration file. When this option is used
the loading of all stages is disabled. Only the given configuration
file is parsed and used. To use another name for the configuration
create a symbolic link of the application or copy it, eg scautopick \-> scautopick2.
.UNINDENT
.INDENT 0.0
.TP
.B \-\-plugins arg
Load given plugins.
.UNINDENT
.INDENT 0.0
.TP
.B \-D, \-\-daemon
Run as daemon. This means the application will fork itself and
doesn\(aqt need to be started with &.
.UNINDENT
.SS Verbosity
.INDENT 0.0
.TP
.B \-\-verbosity arg
Verbosity level [0..4]. 0:quiet, 1:error, 2:warning, 3:info, 4:debug
.UNINDENT
.INDENT 0.0
.TP
.B \-v, \-\-v
Increase verbosity level (may be repeated, eg. \-vv)
.UNINDENT
.INDENT 0.0
.TP
.B \-q, \-\-quiet
Quiet mode: no logging output
.UNINDENT
.INDENT 0.0
.TP
.B \-\-print\-component arg
For each log entry print the component right after the
log level. By default the component output is enabled
for file output but disabled for console output.
.UNINDENT
.INDENT 0.0
.TP
.B \-\-component arg
Limits the logging to a certain component. This option can be given more than once.
.UNINDENT
.INDENT 0.0
.TP
.B \-s, \-\-syslog
Use syslog logging back end. The output usually goes to /var/lib/messages.
.UNINDENT
.INDENT 0.0
.TP
.B \-l, \-\-lockfile arg
Path to lock file.
.UNINDENT
.INDENT 0.0
.TP
.B \-\-console arg
Send log output to stdout.
.UNINDENT
.INDENT 0.0
.TP
.B \-\-debug
Debug mode: \-\-verbosity=4 \-\-console=1
.UNINDENT
.INDENT 0.0
.TP
.B \-\-trace
Trace mode: \-\-verbosity=4 \-\-console=1 \-\-print\-component=1 \-\-print\-context=1
.UNINDENT
.INDENT 0.0
.TP
.B \-\-log\-file arg
Use alternative log file.
.UNINDENT
.SS Messaging
.INDENT 0.0
.TP
.B \-u, \-\-user arg
Overrides configuration parameter \fBconnection.username\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B \-H, \-\-host arg
Overrides configuration parameter \fBconnection.server\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B \-t, \-\-timeout arg
Overrides configuration parameter \fBconnection.timeout\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B \-g, \-\-primary\-group arg
Overrides configuration parameter \fBconnection.primaryGroup\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B \-S, \-\-subscribe\-group arg
A group to subscribe to. This option can be given more than once.
.UNINDENT
.INDENT 0.0
.TP
.B \-\-encoding arg
Overrides configuration parameter \fBconnection.encoding\fP\&.
.UNINDENT
.INDENT 0.0
.TP
.B \-\-start\-stop\-msg arg
Sets sending of a start\- and a stop message.
.UNINDENT
.SS Database
.INDENT 0.0
.TP
.B \-\-db\-driver\-list
List all supported database drivers.
.UNINDENT
.INDENT 0.0
.TP
.B \-d, \-\-database arg
The database connection string, format: \fI\%service://user:pwd@host/database\fP\&.
"service" is the name of the database driver which can be
queried with "\-\-db\-driver\-list".
.UNINDENT
.SH AUTHOR
GFZ Potsdam
.SH COPYRIGHT
GFZ Potsdam, gempa GmbH
.\" Generated by docutils manpage writer.
.
